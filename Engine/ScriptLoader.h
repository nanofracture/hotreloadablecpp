
//////////////////////////////////////////////////////////////////////////
///
/// Copyright (c)
///	@author		Nnanna Kama
///
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
/// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
/// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
/// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////

#include "Windows.h"
#include "ScriptBase.h"
#include <string>
#include <vector>

namespace nanofracture {

	class ScriptEnvironment;

	typedef void*	ScriptDataContext;

	class ScriptLoader
	{
	public:
		ScriptLoader(ScriptEnvironment* pEnv);
		~ScriptLoader();

		ScriptInterface*	Load(const char* pName, ScriptDataContext pCtx, bool pReload = false);

		void				Unload(ScriptInterface*& pScript);

	private:
		bool compileScript(const char *filename, unsigned pVersion);

		ScriptEnvironment*		mEnv;
		class ScriptCollection*	mScripts;
		HANDLE					mProcessReadHandle;
		HANDLE					mProcessWriteHandle;
		char					mVCVars[128];
		unsigned				mVersioning;
	};
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Here onwards can be sectioned off into a cpp file
//

#define NANOFRAC_ASSERT(x)	if((x) == false) { __debugbreak(); }

	class ScriptCollection
	{
		// DISCLAIMER. WARNING. BAD BAD CODE APPROACHING.
		// this function is just here for completeness. NEVER USE IN ACTUAL DEVELOPMENT! use crc32 OR FNV instead.
		static unsigned utterlyhorridhash( const char* pString )
		{
			unsigned val(0);
			while( *pString != '\0' )
			{
				val |= (val << 6) ^ *(pString++);
			}
			return val;
		}
		// END BAD BAD CODE.
		/////////////////////////////////
		
	public:
		void Cache(const char* pName, ScriptInterface* pScript, HMODULE pHandle)
		{
			if (Find(pName) < 0)
			{
				ScriptKey key = { utterlyhorridhash(pName), pScript, pHandle };
				mLoadedScripts.push_back(key);
			}
		}

		int Find(const char* pName) const
		{
			const unsigned id = utterlyhorridhash(pName);
			for (unsigned i = 0; i < mLoadedScripts.size(); ++i)
			{
				if (mLoadedScripts[i].mUID == id)
					return i;
			}
			return -1;
		}

		int Find(ScriptInterface* pScript) const
		{
			for (unsigned i = 0; i < mLoadedScripts.size(); ++i)
			{
				if (mLoadedScripts[i].mScript == pScript)
					return i;
			}
			return -1;
		}

		ScriptInterface* operator[](unsigned i) const
		{
			if (i < mLoadedScripts.size())
				return mLoadedScripts[i].mScript;

			return nullptr;
		}

		void Erase(unsigned i)
		{
			if (i < mLoadedScripts.size())
			{
				::FreeLibrary(mLoadedScripts[i].mModule);
				
				mLoadedScripts[i] = mLoadedScripts.back();
				mLoadedScripts.pop_back();
			}
		}

	private:
		struct ScriptKey
		{
			unsigned			mUID;
			ScriptInterface*	mScript;
			HMODULE				mModule;
		};
		std::vector<ScriptKey>	mLoadedScripts;
	};

	ScriptLoader::ScriptLoader(ScriptEnvironment* pEnv) : mEnv(pEnv), mProcessReadHandle(nullptr), mProcessWriteHandle(nullptr)
	{
		mScripts = new ScriptCollection();

		SECURITY_ATTRIBUTES saAttr = {};
		{
			saAttr.nLength				= sizeof(SECURITY_ATTRIBUTES);
			saAttr.bInheritHandle		= TRUE;										// Set the bInheritHandle flag so pipe handles are inherited.
			saAttr.lpSecurityDescriptor = NULL;
		}

		if (!::CreatePipe(&mProcessReadHandle, &mProcessWriteHandle, &saAttr, 0))	// for accessing spawned process's debug output
			NANOFRAC_ASSERT(0 && "Process error output pipe failure");

#if _MSC_VER == 1800
		const char* Version = "12.0";
#elif _MSC_VER == 1900
		const char* Version = "14.0";
#else
#error add support for your MSVC version
#endif

		const char* keyName = "SOFTWARE\\Microsoft\\VisualStudio\\SxS\\VC7";
		char value[MAX_PATH];
		DWORD size(MAX_PATH);
		HKEY key;
		LONG errCode = ::RegOpenKeyExA(HKEY_LOCAL_MACHINE, keyName, 0, KEY_READ | KEY_WOW64_32KEY, &key);
		errCode |= ::RegQueryValueExA(key, Version, NULL, NULL, (LPBYTE)value, &size);
		if (errCode == ERROR_SUCCESS)
		{
			mVCVars[0] = '"';
			mVCVars[1] = '\0';						//mVCVars = "\"";
			strcat_s(mVCVars, value);				//mVCVars += value;
			strcat_s(mVCVars, "vcvarsall.bat");		//mVCVars += "vcvarsall.bat";
			strcat_s(mVCVars, "\"");				//mVCVars += "\"";
#ifdef _WIN64
			strcat_s(mVCVars, " amd64");			//mVCVars += " amd64";
#else
			strcat_s(mVCVars, " x86");				//mVCVars += " x86";
#endif
		}
	}

	ScriptLoader::~ScriptLoader()
	{
		::CloseHandle(mProcessWriteHandle);
		::CloseHandle(mProcessReadHandle);
	}

	ScriptInterface* ScriptLoader::Load(const char* pName, ScriptDataContext pCtx, bool pReload /*= false*/)
	{
		unsigned version = 0;
		static volatile bool FORCE_GENERATE_DEBUG_LIBS(false);
		if (pReload & FORCE_GENERATE_DEBUG_LIBS)
			version = ++mVersioning;

		const int index = mScripts->Find(pName);
		if (index >= 0)
		{
			if (pReload)
				mScripts->Erase(index);
			else
				return (*mScripts)[index];
		}

		typedef ScriptInterface*(*CreateScript)();
		ScriptInterface* script = nullptr;
		if (compileScript(pName, version))
		{
			std::string fullpath = "modules\\";
			fullpath += pName;
			if (version)
				fullpath += std::to_string(version);
			fullpath += ".dll";
			auto hInstance = ::LoadLibraryA(fullpath.c_str());
			auto creator = (CreateScript)GetProcAddress(hInstance, "CreateScript");
			if (creator)
				script = creator();

			if (script)
			{
				ScriptAttributes rOutAttrib;
				script->Initialise(mEnv, pCtx, rOutAttrib);
				mScripts->Cache(pName, script, hInstance);
			}
		}
		return script;
	}

	void ScriptLoader::Unload(ScriptInterface*& pScript)
	{
		int i = mScripts->Find(pScript);
		if (i >= 0)
		{
			mScripts->Erase(i);
		}

		pScript = nullptr;
	}


	bool ScriptLoader::compileScript(const char *filename, unsigned pVersion)
	{
		bool success = false;
		std::string cmd;
		cmd.reserve(512);
		cmd = mVCVars;
		cmd += " && cl /LD /MD /EHsc /GR- /nologo ";
		if (pVersion)
		{
			cmd += "/Z7 ";	// emit a .pdb debug file
		}

		// include paths
		cmd += "/I \".\\Engine\" ";

		cmd += "Scripts\\";
		cmd += filename;
		cmd += ".cpp";

		//output paths
		if (pVersion)
		{
			std::string incrVer = filename + std::to_string(pVersion);
			cmd += " /Fomodules\\ /Femodules\\" + incrVer;		// apply versioning on output file as VC doesn't unload PDB files.
		}
		else
		{
			cmd += " /Fomodules\\ /Femodules\\";
		}

		// cleanup intermediate files
		{
			std::string removefiles = " && del .\\modules\\";
			removefiles += filename;

			cmd += removefiles + "*.obj";
			cmd += removefiles + "*.lib";
			cmd += removefiles + "*.exp";
			if (!pVersion)
			{
				cmd += removefiles + "*.pdb";	// won't delete access locked files anyway
			}
		}

		STARTUPINFOA si = {};
		PROCESS_INFORMATION pi = {};
		si.cb = sizeof(si);
		si.hStdError = mProcessWriteHandle;
		si.hStdOutput = mProcessWriteHandle;
		si.dwFlags |= STARTF_USESTDHANDLES;
		if (::CreateProcessA(NULL, (LPSTR)cmd.c_str(), NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi) == TRUE)
		{
			DWORD exit_code = 0;
			::WaitForSingleObject(pi.hThread, INFINITE);
			::WaitForSingleObject(pi.hProcess, INFINITE);
			::GetExitCodeProcess(pi.hProcess, &exit_code);
			::CloseHandle(pi.hThread);
			::CloseHandle(pi.hProcess);
			if (exit_code != 0)
			{
#define	ERROR_BUF_SIZE	1024
				DWORD dwRead;
				char chBuf[ERROR_BUF_SIZE];
				BOOL bSuccess = ReadFile(mProcessReadHandle, chBuf, ERROR_BUF_SIZE, &dwRead, NULL);

				if (bSuccess)
				{
					dwRead = dwRead < ERROR_BUF_SIZE ? dwRead : (ERROR_BUF_SIZE - 1);
					chBuf[dwRead] = '\0';
					OutputDebugStringA(chBuf);
				}
				NANOFRAC_ASSERT(0);
			}
			else
			{
				success = true;
			}
		}

		return success;
	}
}