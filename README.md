# README #

A simple Windows project demonstrating runtime compiled and reloadable C++ modules. Built for Visual Studio 2013 or 2015, but can be trivially changed to support other versions. The script: *test.cpp* is used to printf to the console. It can be edited at runtime and reloaded by pressing 'r' with the console window in focus.