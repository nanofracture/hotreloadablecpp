

#include "ScriptBase.h"
#include "ScriptEnvironment.h"

#include "Windows.h"

//////////////////////////////////////////////////////////////////////////
///
/// Copyright (c)
///	@author		Nnanna Kama
///
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
/// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
/// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
/// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#pragma comment(lib,	"kernel32.lib")
#pragma comment(linker, "/entry:_DllMainCRTStartup")
#pragma comment(linker, "/INCREMENTAL:NO")


inline void *__cdecl operator new(size_t, void *_Where)
{
	return (_Where);	/* placement new */
}


using namespace nanofracture;

#define SCRIPT_DEFAULT_MEMBERS				\
	ScriptEnvironment*		mEnv;			\
	ScriptDataContext		mDataContext;	\


#define SCRIPT_ON_INIT				void Initialise(ScriptEnvironment* pEnv, ScriptDataContext pDataContext, ScriptAttributes& rOutAttrib) override

#define SCRIPT_ON_INIT_DEFAULT_BODY	mDataContext = pDataContext; mEnv = pEnv

#define SCRIPT_ON_DESTROY			void Destroy() override

#define SCRIPT_ON_UPDATE			void Update(float pDelta) override

#define SCRIPT_EXPORT(Class)												\
	char sMem[sizeof(Class)];												\
	EXTERN_C __declspec(dllexport) ScriptInterface* CreateScript()			\
	{																		\
		Class* instance = new(sMem) Class;									\
		return instance;													\
	}																		\
	BOOL __stdcall DllMain( HINSTANCE h, DWORD d, LPVOID v ) {return true;}	\
	BOOL __stdcall _DllMainCRTStartup(HINSTANCE h, DWORD d, LPVOID v)		\
	{																		\
		return DllMain(h,d,v);												\
	}																		\

