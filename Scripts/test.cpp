
//////////////////////////////////////////////////////////////////////////
///
/// Copyright (c)
///	@author		Nnanna Kama
///
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
/// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
/// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
/// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////

#include "ScriptHeader.h"

struct TestScript : public ScriptInterface
{
	SCRIPT_ON_INIT
	{
		SCRIPT_ON_INIT_DEFAULT_BODY;
	}

	SCRIPT_ON_DESTROY
	{}

	SCRIPT_ON_UPDATE
	{
		int& elapsedFrames = *(int*)mDataContext;
		++elapsedFrames;

		const bool use_int_format = true;
		//use_int_format = false;									// demo line. Comment/uncomment, save, press reload key.
		if ( use_int_format == false )
		{
			float scalar = elapsedFrames;
			printf("frames elapsed %f\r", scalar);
		}
		else
		{
			printf("frames elapsed %d       \r", elapsedFrames);	// added dead space for clearing pre-existing float decimals.
		}

		::Sleep(300);												// another demo line. artificially slowdown down execution rate. change value.save.reload.
	}

private:
	SCRIPT_DEFAULT_MEMBERS;
};


SCRIPT_EXPORT(TestScript)