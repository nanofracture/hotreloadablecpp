
#include "Engine\ScriptLoader.h"
#include "conio.h"
#include <thread>


#define ESC_KEY		27
#define RELOAD_KEY1	'r'
#define RELOAD_KEY2 'R'

static char gInputMarker			= 0;

bool isKeyPressed( char key )
{
	if (gInputMarker == key)
	{
		gInputMarker = 0;
		return true;
	}
	return false;
}

void inputListenerProcess()
{
	while (true)
	{
		gInputMarker = _getch();
		if (gInputMarker == ESC_KEY)
			break;
	}
}


int main(int argc, char* argv[])
{
	std::thread asyncInputListener{ inputListenerProcess };

	nanofracture::ScriptLoader sl(nullptr);
	nanofracture::ScriptInterface* script = nullptr;
	int framesElapsed(0);

	while ( !isKeyPressed(ESC_KEY) )
	{
		bool reload = isKeyPressed( RELOAD_KEY1 ) || isKeyPressed( RELOAD_KEY2 );
		if (script == nullptr || reload)
		{
			script = sl.Load("test", &framesElapsed, reload);
		}

		script->Update(0.013f);
	}

	asyncInputListener.join();

	sl.Unload(script);

	return 0;
}

